# .NET Simple Merge Sample #
A simple C# example of how to call the XML Merge REST API.

This is the simplest possible sample of how to call the Merge REST API from C# and .NET.
Please note that this method uses a simple XML Request POST, and requires that you have the input files to be merged available on the server.
This means you probably will only use this if your server and client are the same machine, or on the same intranet.

This sample is deliberately kept simple, with minimal error handling, to concentrate on the useful information. 

Minimum Requirements:
.NET Core 2.0 or later
Visual Studio Code (with extension: C# for Visual Studio Code)
Operating System: Windows v7 or later, MacOS Sierra or later, Linux (see Visual Studio documentation for supported versions of Linux).
