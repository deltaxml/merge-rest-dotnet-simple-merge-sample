﻿// Copyright (c) 2024 Deltaman group limited. All rights reserved.

using System;
using System.Net;
using System.IO;
using System.Text;

namespace DotNetSimpleMergeSample
{
    /// <summary>
    /// This is the simplest possible sample of how to call the Merge REST API from C# and .NET.
    /// Please note that this method using simple XML Request POST requires that you have the input files to be merged available on the server.
    /// This means you probably will only use this if your server and client are the same machine, or on the same intranet.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            // Create a request using a URL that can receive a post.
            // We will use locahost. Now edit the input.xml file in the data folder,
            // TODO: Ensure the paths of the files referenced in the <path> elements are correct for your server.
            WebRequest request = WebRequest.Create(@"http://localhost:8080/api/xmlmerge/v1/types/concurrent");
            // Set the Method property of the request to POST.
            request.Method = "POST";
            // Create POST data from an XML config file and convert it to a byte array.
            String postData = File.ReadAllText(@"./data/input.xml");
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            // IMPORTANT: Set the ContentType property of the WebRequest to be "application/xml".
            request.ContentType = "application/xml";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Write the content to the local file result.xml.
            // TODO: Change this path to where you want the merge result to go on your client.
            File.WriteAllText(@"./data/result.xml", responseFromServer);
            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();
        }
    }
}
